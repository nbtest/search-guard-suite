package com.floragunn.searchguard.sgconf;

public interface Initializable {
    
    boolean isInitialized();

}
